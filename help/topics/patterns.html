<title>TinyFugue: patterns</title>
<!--"@patterns"-->
<h1>patterns</h1>

<p>
  Patterns are used in <a href="../topics/triggers.html">triggers</a>,
  <a href="../topics/hooks.html">hooks</a>,
  <a href="../commands/purge.html">/purge</a>,
  <a href="../commands/list.html">/list</a>, and
  <a href="../commands/recall.html">/recall</a>.  There are three styles of
  pattern matching available: "simple" comparison, "glob" (similar to shell
  filename patterns), and "regexp" (regular expressions).  The style used by
  a particular command is determined either by the use of the -m option or
  the setting of the global <a href="../topics/variables.html">variable</a>
  <a href="../topics/special_variables.html#matching">%{matching}</a>.

<p>
<a name="comparison"></a>
<a name="simple"></a>
<a name="simple matching"></a>
<h2>Simple matching ("simple")</h2>

<p>
  The pattern is compared directly to the string.  There are no special
  characters.  Case is significant.

<p>
<a name="smatch"></a>
<a name="globbing"></a>
<a name="glob"></a>
<h2>Globbing ("glob")</h2>

<p>
  Globbing is the default matching style, and was the only style available
  before version 3.2.  It is similar to filename expansion ("globbing") used
  by many shells (but is only used for comparison, not expansion).

<p>
  There are several special sequences that can be used in tf globbing:

<p>
  <ul>
  <li> The '*' character matches any number of characters.

<p>
  <li> The '?' character matches any one character.

<p>
  <li> Square brackets ([...]) can be used to match any one of a sequence of
  characters.  Ranges can be specified by giving the first and last
  characters with a '-' between them.  If '^' is the first character, the
  sequence will match any character NOT specified.

<p>
  <li> Curly braces ({...}) can be used to match any one of a list of words.
  Different words can be matched by listing each within the braces, separated
  by a '|' (or) character.  Both ends of {...} will only match a space or end
  of string.  Therefore "{foo}*" and "{foo}p" do not match "foop", and
  "*{foo}" and "p{foo}" do not match "pfoo".

<p>
  Patterns containing "{...}" can easily be meaningless.  A valid {...}
  pattern must: (a) contain no spaces, (b) follow a wildcard, space, or
  beginning of string, (c) be followed by a wildcard, space, or end of
  string.

<p>
  The pattern "{}" will match the empty string.

<p>
  <li> Any other character will match itself, ignoring case.  A special
  character can be made to match itself by preceding it with '\' to remove
  its special meaning.
  </ul>

<p>
  Examples:<br>
  "<code>d?g</code>" matches "dog", "dig" and "dug" but not "dg" or "drug".<br>
  "<code>d*g</code>" matches "dg", "dog", "drug", "debug", "dead slug", etc.<br>
  "<code>{d*g}</code>" matches "dg", "dog", "drug", "debug", but not "dead slug".<br>
  "<code>M[rs].</code>" matches "Mr." and "Ms."<br>
  "<code>M[a-z]</code>" matches "Ma", "Mb", "Mc", etc.<br>
  "<code>[^a-z]</code>" matches any character that is not in the English alphabet.<br>
  "<code>{storm|chup*}*</code>" matches "chupchup fehs" and "Storm jiggles".<br>
  "<code>{storm|chup*}*</code>" does NOT match "stormette jiggles".<br>

<p>
<a name="re"></a>
<a name="regex"></a>
<a name="regexp"></a>
<a name="regexps"></a>
<a name="regular expressions"></a>
<h2>Regular expressions ("regexp")</h2>

<p>
  The regexp package was written by Henry Spencer, and is similar to those
  used in egrep and many text editors.  See also:
  <a href="../topics/functions.html#regmatch">regmatch()</a>,
  <a href="../topics/substitution.html">substitution</a>.  The following
  excerpt is taken from Henry Spencer's regexp(3) man page.
<pre>
     REGULAR EXPRESSION SYNTAX
          A regular expression is zero or more branches, separated by
          `|'.  It matches anything that matches one of the branches.

          A branch is zero or more pieces, concatenated.  It matches a
          match for the first, followed by a match for the second,
          etc.

          A piece is an atom possibly followed by `*', `+', or `?'.
          An atom followed by `*' matches a sequence of 0 or more
          matches of the atom.  An atom followed by `+' matches a
          sequence of 1 or more matches of the atom.  An atom followed
          by `?' matches a match of the atom, or the null string.

          An atom is a regular expression in parentheses (matching a
          match for the regular expression), a range (see below), `.'
          (matching any single character), `^' (matching the null
          string at the beginning of the input string), `$' (matching
          the null string at the end of the input string), a `\'
          followed by a single character (matching that character), or
          a single character with no other significance (matching that
          character).

          A range is a sequence of characters enclosed in `[]'.  It
          normally matches any single character from the sequence.  If
          the sequence begins with `^', it matches any single
          character not from the rest of the sequence.  If two
          characters in the sequence are separated by `-', this is
          shorthand for the full list of ASCII characters between them
          (e.g. `[0-9]' matches any decimal digit).  To include a
          literal `]' in the sequence, make it the first character
          (following a possible `^').  To include a literal `-', make
          it the first or last character.

     AMBIGUITY
          If a regular expression could match two different parts of
          the input string, it will match the one which begins
          earliest.  If both begin in the same place    but match
          different lengths, or match the same length in different
          ways, life gets messier, as follows.

          In general, the possibilities in a list of branches are
          considered in left-to-right order, the possibilities for
          `*', `+', and `?' are considered longest-first, nested
          constructs are considered from the outermost in, and
          concatenated constructs are considered leftmost-first.  The
          match that will be chosen is the one that uses the earliest
          possibility in the first choice that has to be made.  If
          there is more than one choice, the next will be made in the
          same manner (earliest possibility) subject to the decision
          on the first choice.  And so forth.

          For example, `(ab|a)b*c' could match `abc' in one of two
          ways.  The first choice is between `ab' and `a'; since `ab'
          is earlier, and does lead to a successful overall match, it
          is chosen.  Since the `b' is already spoken for, the `b*'
          must match its last possibility-the empty string-since it
          must respect the earlier choice.

          In the particular case where no `|'s are present and there
          is only one `*', `+', or `?', the net effect is that the
          longest possible match will be chosen.  So `ab*', presented
          with `xabbbby', will match `abbbb'.  Note that if `ab*' is
          tried against `xabyabbbz', it will match `ab' just after
          `x', due to the begins-earliest rule.  (In effect, the
          decision on where to start the match is the first choice to
          be made, hence subsequent choices must respect it even if
          this leads them to less-preferred alternatives.)
</pre>
<h2>Comparison of glob and regexps.</h2>

In a glob, '*' and '?' by themselves match text.
In a regexp, '*' and '?' are only meaningful in combination with
the pattern they follow.

Regexps are not "anchored"; that is, the match may occur anywhere in
the string, unless you explicitly use '^' and/or '$' to anchor it.
Globs are anchored, and must match the entire string.

<pre>
    regexp		equivalent glob
			(except for case)
    ------		-----------------
    "part of line"	"*part of line*"
    "^entire line$"	"entire line"
    "(^| )word( |$)"	"*{word}*"
    "^(You|Hawkeye) "	"{You|Hawkeye} *"
    "foo.*bar"		"*foo*bar*"
    "f(oo|00)d"		"*{*food*|*f00d*}*"
    "line[0-9]"		"*line[0-9]*"
    "^[^ ]+ whispers,"	"{*} whispers,*"
    "foo(AB)?bar"	"*{*foobar*|*fooABbar*}*"
    "zoo+m"		none
    "foo ?bar"		none
    "(foo bar|frodo)"	none
</pre>

<p>
<h2>Notes.</h2>
<ul>
<li>For best speed, make the beginning of your patterns as specific as possible.
<li>Do not use "<code>.*</code>" or "<code>^.*</code>" at the beginning of a
regexp.  It is <em>very</em> inefficient, and not needed.  Use
<a href="../topics/substitution.html#%Pn">%PL</a> instead if you need to
retrieve the value of the left side.
<li>If a glob and regexp can do the same job, use the glob, it's usually faster.
</ul>

<p>
<!-- END -->
<hr>
  <a href="./">Back to index</a><br>
  <a href="http://tf.tcp.com/~hawkeye/tf/">Back to tf home page</a>
<hr>
  <a href="../topics/copyright.html">Copyright</a> &copy; 1995 - 1999 <a href="http://tf.tcp.com/~hawkeye/">Ken Keys</a>
