<title>TinyFugue: status line</title>
<!--"@status"-->
<!--"@status fields"-->
<!--"@status_fields"-->
<!--"@%status_fields"-->
<!--"@visual bar"-->
<!--"@visual line"-->
<!--"@status bar"-->
<!--"@status_line"-->
<!--"@status line"-->
<h1>status line</h1>

  In <a href="../topics/mode.html#visual">visual</a> mode,
  the input and output windows are separated by a status line, which
  by default looks something like this:
<pre>
  <strong>More 156</strong>_<var>WorldName</var>____________(Read)_(Active: <var>n</var>)_(Log)_(Mail)_(Over)_12:34
</pre>
  <ul>
  <li>"<a href="../commands/more.html">More</a>" indicates how many
      <a href="../commands/more.html">more</a> lines of text are waiting
      to be seen.
  <li>"<i>WorldName</i>" is the name of the
      <a href="../topics/sockets.html#foreground">foreground</a>
      <a href="../topics/sockets.html">socket</a>'s world.
  <li>"(Read)" indicates that keyboard input is being read by
      <a href="../topics/read.html">read()</a>.
  <li>The "(Active: <var>n</var>)" indicator shows the number of
      <a href="../topics/sockets.html">sockets</a> with unseen text.
  <li>"(Log)" indicates that there is one or more
      <a href="../commands/log.html">log</a> file open.
  <li>"(Mail)" or "Mail <var>n</var>" indicates the number of files named by
      <a href="../topics/mail.html">%MAIL</a> or
      <a href="../topics/mail.html">%MAILPATH</a>
      that contain unread mail.
  <li>"(Over)" indicates that typed characters will overstrike instead of
      insert (that is,
      <a href="../topics/special_variables.html#insert">%insert</a> is off).
  <li>The current time is displayed at the right end of the status line.
  </ul>


<h1>Configuring the status line</h1>

<p>
  The <a href="../topics/status_line.html">status_fields</a>
  <a href="../topics/variables.html">variable</a>
  contains a list of descriptions of fields to be displayed on the status
  line.  Each description consists of:
  <ul>
     <li>an optional field name
     <li>an optional ":" and number indicating the field width
     <li>an optional ":" and <a href="../topics/attributes.html">attribute</a>
  </ul>

<p>
  Fields without names are used to indicate padding between named fields.
  Field names enclosed in quotes (", ', or `) are displayed literally; use
  the \ character to escape a quote inside the string.
  Field names beginning with "@" correspond to internal states.
  Other field names correspond to the
  <a href="../topics/variables.html">variable</a> with the same name.
  TF will monitor the internal state or
  <a href="../topics/variables.html">variable</a> corresponding to
  each internal and <a href="../topics/variables.html">variable</a>
  field, and update those fields whenever the
  monitored item changes.
  Also, the entire status line will be updated whenever
  the screen is redrawn or the
  <a href="../topics/status_line.html">status_fields</a>
  <a href="../topics/variables.html">variable</a>
  is modified.

<p>
  The actual format of an internal or
  <a href="../topics/variables.html">variable</a> field displayed on the
  status line is determined by evaluating the
  <a href="../topics/expressions.html">expression</a> contained in the
  <a href="../topics/variables.html">variable</a>
  <code>status_int_<i>name</i></code> (for internal state
  <code>@<i>name</i></code>) or
  <code>status_var_<i>name</i></code> (for variable <code><i>name</i></code>).
  Also, for <a href="../topics/variables.html">variable</a>
  fields, if <code>status_var_<i>name</i></code> is not
  set, the value of the <a href="../topics/variables.html">variable</a>
  will be displayed directly.  Changing a format variable will not cause
  the status line to update; to force an update, you can
  "<code><a href="../commands/dokey.html">/dokey</a> redraw</code>" (^L), or
  "<code><a href="../commands/test.html">/test</a> 
  <a href="../topics/status_line.html">status_fields</a> :=
  <a href="../topics/status_line.html">status_fields</a></code>".

<p>
  A field's width determines how many columns it will take up on the screen.
  If the width of a string literal field field is omitted, it defaults to the
  length of the string literal.
  One other field width may be omitted, which means that field will use
  whatever columns are unused by the other fields.
  Normally, fields are left-justified within the width, but a negative
  field width will right-justify the field within the absolute
  value of the width.  If the formatted text is wider than the field width,
  it will be truncated to fit within the specified width.  Fields may also
  be truncated if they would not fit on the screen.

<p>
  The <a href="../topics/attributes.html">attributes</a> are applied to the
  field text when it is displayed, but are not applied to the padding used to
  bring the field to the specified width.

<p>
  Any <a href="../topics/variables.html">variable</a> may be monitored,
  but there is a fixed list of internal statuses.
  The internal statuses available are:
  <dl>
  <dt><code>@more</code>
	<dd>Updated when the number of unseen lines changes.
  <dt><code>@world</code>
	<dd>Updated when when the
	<a href="../topics/sockets.html">foreground</a>
	<a href="../topics/worlds.html">world</a> changes.
	During the format expression, the
        <a href="../topics/sockets.html">current socket</a> is the
	new <a href="../topics/sockets.html">socket</a>.
  <dt><code>@read</code>
	<dd>Updated when entering or exiting a
	<a href="../topics/read.html">read()</a> function call.
  <dt><code>@active</code>
	<dd>Updated when the number of active
	<a href="../topics/worlds.html">worlds</a> changes.
	During the format expression, the
        <a href="../topics/sockets.html">current socket</a> is the
	<a href="../topics/sockets.html">socket</a> that became active.
  <dt><code>@log</code>
	<dd>Updated when the number of open
	<a href="../commands/log.html">log</a> files changes.
  <dt><code>@mail</code>
	<dd>Updated when mail arrives (See
	"<a href="../topics/mail.html">mail</a>").
  <dt><code>@clock</code>
	<dd>Updated every minute, at 0 seconds past the minute.
  </dl>

<p>
  The entire status line, including padding, is displayed with the
  <a href="../topics/attributes.html">attributes</a> given by
  <a href="../topics/special_variables.html#status_attr">%status_attr</a>,
  which is none by default.  Individual field
  <a href="../topics/attributes.html">attributes</a> are combined with
  <a href="../topics/special_variables.html#status_attr">%status_attr</a>
  <a href="../topics/attributes.html">attributes</a>.

<p>
  To bring fields up to their specified width, they are padded with 
  <a href="../topics/special_variables.html#status_pad">%status_pad</a>,
  which is "_" by default.  By setting 
  <a href="../topics/special_variables.html#status_pad">status_pad</a>
  to " " and
  <a href="../topics/special_variables.html#status_attr">status_attr</a>
  to "r", you can create a status line that looks more like the one in emacs
  or the irc client.

<p>
  All this may sound rather complex, so an example might help.  The default
  value of <a href="../topics/status_line.html">status_fields</a> is:

  <pre>
  @more:8:Br :1 @world :1 @read:6 :1 @active:11 :1 @log:5 :1 @mail:6 :1 insert:6 :1 @clock:5
  </pre>

  and the corresponding format
  <a href="../topics/variables.html">variables</a> are:

  <pre>
  <a href="../commands/set.html">/set</a> status_int_more \
       <a href="../topics/functions.html#moresize">moresize()</a> == 0 ? "" : \
       <a href="../topics/functions.html#moresize">moresize()</a> &gt; 9999 ? "MuchMore" : \
       <a href="../topics/functions.html#pad">pad</a>("More", 4, <a href="../topics/functions.html#moresize">moresize()</a>, 4)
  <a href="../commands/set.html">/set</a> status_int_world   <a href="../topics/worlds.html">${world_name}</a>
  <a href="../commands/set.html">/set</a> status_int_read    <a href="../topics/functions.html#nread">nread()</a> ? "(Read)" : ""
  <a href="../commands/set.html">/set</a> status_int_active  <a href="../topics/functions.html#nactive">nactive()</a> ? <a href="../topics/functions.html#pad">pad</a>("(Active:",0,<a href="../topics/functions.html#nactive">nactive()</a>,2,")") : ""
  <a href="../commands/set.html">/set</a> status_int_log     <a href="../topics/functions.html#nlog">nlog()</a> ? "(Log)" : ""
  <a href="../commands/set.html">/set</a> status_int_mail \
       !<a href="../topics/functions.html#nmail">nmail</a>() ? "" : \
       <a href="../topics/functions.html#nmail">nmail</a>()==1 ? "(Mail)" : \
       <a href="../topics/functions.html#pad">pad</a>("Mail", 0, <a href="../topics/functions.html#nmail">nmail</a>(), 2)
  <a href="../commands/set.html">/set</a> status_var_insert  <a href="../topics/special_variables.html#insert">insert</a> ? "" : "(Over)"
  <a href="../commands/set.html">/set</a> status_int_clock   <a href="../commands/ftime.html">ftime</a>("%I:%M", <a href="../topics/functions.html#time">time()</a>)
  </pre>

<p>
  The first field is "<code>@more:8:Br</code>".  So, whenever the number of
  unseen lines changes, TF looks for the
  <a href="../topics/variables.html">variable</a>
  <code>status_int_more</code>, and evaluates the
  <a href="../topics/expressions.html">expression</a> it contains.
  The result of the <a href="../topics/expressions.html">expression</a>
  is printed in the first 8 columns of the status line, with
  <a href="../topics/attributes.html">attributes</a> "Br" (bold and reverse).
  The <a href="../topics/expressions.html">expression</a> was carefully
  written so that it will never be more than 8 characters, because it would be
  confusing to generate something like "<code>More:12345</code>" and then have
  it truncated to "<code>More:123</code>" because of the field width of 8.

<p>
  Since the "<code>@world</code>" field has no explicit width, its width
  is determined dynamically.
  The fields on its left are pushed to the left side of the screen,
  the fields on its right are pushed to the right side of the screen, and
  the "<code>@world</code>" field uses whatever space remains in the middle.

<p>
  The unnamed fields with width 1 are used to display a pad character between
  the named fields.

<a name="prompt example"></a><!-- referenced by ../commands/prompt.html -->
<p>
  Another example:  Say your mud has a
  <a href="../topics/prompts.html">prompt</a> like
  "<code>H:42 M:17&gt; </code>" that shows your hit points and mana,
  and you want it displayed on the status line like
  "<code> 42, 17</code>".
  To do this, add "<code>hp_mana:7</code>" to the
  <a href="../topics/status_line.html">status_fields</a>
  <a href="../topics/variables.html">variable</a>,
  and define a <a href="../topics/prompts.html">prompt</a>
  <a href="../topics/hooks.html">hook</a>:
<pre>
  <a href="../commands/def.html">/def</a> <a href="../commands/def.html#-m">-mregexp</a> <a href="../commands/def.html#-h">-h</a>"PROMPT ^H:([^ ]*) M:([^ ]*)&gt; $" hp_mana_hook = \
      <a href="../commands/set.html">/set</a> hp=<a href="../topics/substitution.html#%Pn">%P1</a>%; \
      <a href="../commands/set.html">/set</a> mana=<a href="../topics/substitution.html#%Pn">%P2</a>%; \
      <a href="../commands/set.html">/set</a> hp_mana=<a href="../topics/substitution.html#$[">$[</a><a href="../topics/functions.html#pad">pad</a>(hp, 3, ",", 0, mana, 3)]%; \
      <a href="../commands/test.html">/test</a> <a href="../commands/prompt.html">prompt</a>(<a href="../topics/substitution.html#%*">{*}</a>)
</pre>
<a name=""></a>

<p>
  See: <a href="../topics/mode.html#visual">visual</a>

<p>
<!-- END -->
<hr>
  <a href="./">Back to index</a><br>
  <a href="http://tf.tcp.com/~hawkeye/tf/">Back to tf home page</a>
<hr>
  <a href="../topics/copyright.html">Copyright</a> &copy; 1995 - 1999 <a href="http://tf.tcp.com/~hawkeye/">Ken Keys</a>
